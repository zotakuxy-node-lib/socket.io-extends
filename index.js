(()=>{
    const { AuthIO, K } = require( './lib/socket.io.auth' );
    const { listenAny } = require( './lib/socket.io.listen-any' );
    module.exports = { AuthIO, K, listenAny };
})();