(()=>{
    const { listenAny } = require( './socket.io.listen-any' );
    const { immutable, immutableCascade } = require ( 'zoo.util/lib/object-util').ObjectUtil

    /**
     * @param io { Server }
     * @param anyEventListener
     * @constructor
     */
    const K = {
        AUTHENTICATION : "authentication",
        AUTHENTICATION_ACCEPT : "authentication:accept",
        AUTHENTICATION_REJECT : "authentication:reject",
        AUTHENTICATION_CANCELED : "authentication:canceled",
        METHOD : "method"
    };

    /**
     *
     * @param socket { Socket }
     * @param logout { function( AuthSession  ):* }
     * @constructor
     */
    const AuthSession = function ( socket, logout ) {
        /** @type {Socket}*/
        this.socket = socket;
        this.authenticated =  false;
        this.authenticationWait =  false;
        this.method =  null;
        this.data = null;
        this.extras = { };
        this.protect = [];
        this.session = null;
        this.logout = () =>{ logout( this )};
        immutable( this, "logout" );
    };

    const protectMethods = [ "emit", "join", "on", "once" ];

    /**
     * @param socket { Socket}
     * @param auth { AuthSession }
     */
    const oneShot = ( socket, auth )=>{
        return ( method )=>{
            return ( ...data) =>{
                const current = socket[ method ];
                socket[ method ] = auth.protect[ method ];
                socket[ method ] ( ...data );
                socket[ method ] = current;
            }
        }
    };

    /**
     * @param socket { Socket }
     * @param auth { AuthSession }
     */
    const protect= ( socket, auth )=>{
        protectMethods.forEach( fName => {
            auth.protect[ fName ] = socket[ fName ];
            socket[ fName ] = ()=>{ return new Error(`Permission deaned on ${fName} of socket, socket is not authenticated!`)};
        });
        return { oneShot: oneShot( socket, auth ) }
    };

    /**
     * @param socket { Socket }
     * @param auth { AuthSession }
     */
    const unprotect= ( socket, auth )=>{
        protectMethods.forEach( fuc => {
            socket[ fuc ] = auth.protect[ fuc ];
        });
    };

    /**
     * @param socket { Socket }
     * @param auth { AuthSession }
     * @param response
     */
    const reject = ( socket, auth, response )=>{
        if( !response ) response = {};
        else if( typeof response !== "object" ) response = { response: response };
        response.result = false;
        if( !response.message ) response.message = "access denied";
        response.code = K.AUTHENTICATION_REJECT;
        oneShot( socket, auth )("emit" )( K.AUTHENTICATION_REJECT, response );
        oneShot( socket, auth )("emit" )( K.AUTHENTICATION, response );
        socket[ "emit" ] = auth.protect[ "emit" ];
        socket.disconnect();
    };




    const logout = ( self) => {
        self.authenticated = false;
        self.authenticationWait = false;
        unprotect( self.socket, self );
        self.socket.disconnect();
    };


    /**@const*/
    class AuthIO {

        /**@type {{callback:function( authData:*, socket:Socket)}[]}*/

        #_methods = [];
        /**@type {function( socket:Socket, response, auth: AuthSession)[][]}*/
        #_onAuthenticated = [];

        /**@type Server*/
        #_io;

        /**@type {function( authMethod:string, socket:Socket, response, auth: AuthSession)[]}*/
        #_onAuthenticatedAny = [];
        #_timeout;

        constructor( io, anyEventListener ) {
            listenAny.io( io, anyEventListener );
            this.#_io = io;

            io.on( 'connection', /** @param socket { Socket }*/ ( socket ) =>{
                const auth = new AuthSession( socket, logout );
                const { oneShot } = protect( socket, auth );

                oneShot( "on" )(  anyEventListener, ( event, ...data ) =>{
                    if( !auth.authenticated && event !== K.AUTHENTICATION ){
                        reject( socket, auth, {
                            message: `event "${ event }" rejected. Cannot emit any event before ${ K.AUTHENTICATION }`
                        });
                    }
                });

                oneShot( 'on')( 'disconnect', ()=>{
                    auth.authenticated = false;
                    auth.authenticationWait = false;
                });

                oneShot("on" )( K.AUTHENTICATION, ( authData )=>{

                    if( auth.authenticated ) {
                        const response = {
                            message: 'Socket already authenticated',
                            code: K.AUTHENTICATION_CANCELED,
                            result: false
                        };

                        socket.emit( K.AUTHENTICATION_CANCELED, response );
                        socket.emit( K.AUTHENTICATION,  response );
                        return;
                    }

                    let response = (()=>{
                        auth.authenticationWait = true;
                        if( !authData ) return { result: false, message: "missing auth arguments" };
                        if( !authData[ K.METHOD ] ) return { result: false, message: "missing method in auth arguments" };
                        if( !this.#_methods[ authData[ K.METHOD ] ] ) return { result: false, message: "unknown auth method" };
                        auth.method = authData[ K.METHOD ];
                        auth.data = authData;
                        
                        immutableCascade( auth.data );
                        immutable( auth, 'data' );
                        immutable( auth, 'extras' );
                        return this.#_methods[ authData[ K.METHOD ] ].callback( authData, socket );
                    })();

                    if( response && response.result ) this.#_accept( socket, auth );
                    else if( typeof response === "boolean" && response ) this.#_accept( socket, auth, response );
                    else {
                        if( typeof response === "boolean" ) response = {
                            result: false,
                            message: "Rejected by method"
                        };

                        reject( socket, auth, response );
                    }
                });

                if( this.#_timeout ){
                    setTimeout( ()=>{
                        if( !auth.authenticated && !auth.authenticationWait ) reject( socket, auth, {
                            message: `Timeout ${ this.#_timeout } to emit authentication intent exceeded`
                        })
                    }, this.#_timeout );
                }
            });

        }

        get timeout() { return this.#_timeout; }
        get io() { return this.#_io; }

        set timeout( timeout ){ this.#_timeout = timeout; }

        /**
         * @param socket { Socket }
         * @param auth { AuthSession }
         * @param response
         */
        #_accept = ( socket, auth, response )=>{
            unprotect( socket, auth );

            auth.authenticated = true;
            auth.authenticationWait = false;
            auth.session = {
                key: new Date().getTime()+""+ (Math.random() * 9999).toFixed()
            };

            if( !response ) response = {};
            else if( typeof response !== "object" ) response = { response: response };
            response.result = true;
            response.session =  auth.session;
            if( !response.message ) response.message = "successfully";

            response.code = K.AUTHENTICATION_ACCEPT;
            socket.emit( K.AUTHENTICATION_ACCEPT, response);
            socket.emit( K.AUTHENTICATION, response);
            const listeners = this.#_onAuthenticated[ auth.method ];
            if( listeners ){
                listeners.forEach( ( next )=>{
                    next( socket, response, auth );
                })
            }

            this.#_onAuthenticatedAny.forEach( next => {
                next( auth.method, socket, response, auth );
            })
        };

        /**
         * @param authMethod { String | String[] }
         * @param callback { function( socket:Socket, response, auth:AuthSession ):*}
         */
        onAuthenticated ( authMethod, callback ){
            if( typeof authMethod === "function" ) throw new Error( "authMethod nao pode ser funcao" );
            if( typeof authMethod === "string" ) authMethod = [ authMethod ];
            authMethod.forEach( value => {
                let listener;
                if( !( listener = this.#_onAuthenticated[ value ]) ) {
                    listener = [];
                    this.#_onAuthenticated[ authMethod ] = listener;
                }
                listener.push( callback );
            });
        };

        /**
         * @param name { String }
         * @param method { function( authData:*, socket:Socket ): boolean }
         */
        defineMethod ( name, method ) {
            const define = [];
            define[ name ] = method;
            this.method = define;
        }

        /**
         * @param callback { function( authMethod:string, socket:Socket, response, auth:AuthSession ):*}
         */
        onAuthenticatedAny (  callback ){
            this.#_onAuthenticatedAny.push( callback );
        };


        /** @param methods { {function( authData, socket: Socket )}[] }*/
        set method ( methods ){
            if( typeof methods !== "object" ) return false;
            Object.keys( methods ).forEach(methodName =>{
                if(!this.#_methods[ methodName ])
                    this.#_methods[ methodName ] = { };

                if( typeof methods[ methodName ] === "function")
                    this.#_methods[ methodName ].callback =  methods[ methodName ];
            });
        };
    }

    module.exports = { AuthIO, K };
})();
