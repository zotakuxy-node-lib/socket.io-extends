(()=>{
    require( 'zoo.util/lib/array-util' ).ArrayUtil.extendsMethods();
    const { immutable } = require( 'zoo.util/lib/object-util' ).ObjectUtil;

    /**
     * @constructor
     */
    const ListenAny = function () {
        const _io = [];
        const _socket =  [];

        this.io = ( io, anyEvent ) =>{
            if ( _io.indexOf( io ) !== -1 ) return false;
            io.on( 'connection', ( socket ) =>{
                this.socket( socket, anyEvent );
            });

            _io.push( io );
            return true;
        };

        this.socket = ( socket, anyEvent ) =>{
            if( _socket.indexOf( socket ) !== -1 ) return false;
            const onevent = socket.onevent;
            socket.onevent = function ( packet ) {
                const args = packet.data || [];
                onevent.call ( this, packet );    // original call
                packet.data = [ anyEvent ].concat( args );
                onevent.call( this, packet );      // additional call to catch-all
            };

            _socket.push( socket );
            return true;
        };
    };

    const listenAny = new ListenAny();
    immutable( listenAny );
    module.exports = { listenAny };
})();
